# OpenAPI schema for SSO Management API

SSO Management API uses path based variable length identifiers that are not easily expressed with OpenAPI schema and/or the Swagger toolset. 

OpenAPI supports parts of [RFC 6570 URI Template](https://www.rfc-editor.org/rfc/rfc6570) specification but lacks support for [Path Segment Expansion](https://www.rfc-editor.org/rfc/rfc6570#section-3.2.6). This feature allows expressing variable length path segments using an expression like `{/var*}`.

The OpenAPI schema files referenced below are simplified and require the API client to implement support for variable length path segments.

## sso-api.json

https://petstore.swagger.io/?url=https://bitbucket.org/jeps/sso-api-schema/raw/master/sso-api.json

Quite large schema that includes almost all features of SSO API.

The variable length path segments are expressed as `{path}` and `{link-path}`. 

### Path expression examples

The `site` object path is `GET /site/{path}`. The `{path}` part is variable length and could include any number of segments: `GET /site/Example` or `GET /site/Example/Subsite`.

The `application` object path is `GET /application/{path}/{name}`. The `{path}` part is variable length and could include any number of segments: `GET /application/Example/app1` or `GET /application/Example/Subsite/app1`. The `{name}` part is a single segment.

The `policyItem` object path is `GET /policyItem/{path}/{parent}/{name}`. The `{path}` part is variable length and could include any number of segments: `GET /policyItem/Example/policy1/item1` or `GET /policyItem/Example/Subsite/policy1/item1`. Because `policyItem` is always child of `policy` the `{parent}` part is single segment name of parent. The `{name}` part is a single segment.

## application.json

https://petstore.swagger.io/?url=https://bitbucket.org/jeps/sso-api-schema/raw/master/application.json

Subset of `sso-api.json` with features related to application only.

## credential.json

https://petstore.swagger.io/?url=https://bitbucket.org/jeps/sso-api-schema/raw/master/credential.json

Subset of `sso-api.json` with features related to credential only.

## links.json

https://petstore.swagger.io/?url=https://bitbucket.org/jeps/sso-api-schema/raw/master/links.json

Work in progress. Testing schema with path expression `{/objectId*}`

## simple.json

https://petstore.swagger.io/?url=https://bitbucket.org/jeps/sso-api-schema/raw/master/simple.json

Work in progress. Testing schema with path expression `{/objectId*}` and `{/linkId*}`

