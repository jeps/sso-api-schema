{
  "openapi": "3.0.0",
  "info": {
    "title": "SSO Management API",
    "version": "1.0"
  },
  "servers": [
    {
      "url": "https://manage.example.ubidemo.com/sso-api",
      "description": "example.ubidemo.com"
    }
  ],
  "components": {
    "schemas": {
      "ObjectType": {
        "type": "string",
        "description": "object types",
        "enum": [
          "site",
          "application",
          "group",
          "method",
          "server",
          "user",
          "policy",
          "policyItem",
          "outboundMappingPolicy",
          "outboundUserMapping",
          "refreshTokenPolicy",
          "services",
          "directory",
          "service",
          "inboundMappingPolicy",
          "inboundDirectoryMapping",
          "inboundServiceMapping",
          "inboundPolicy",
          "inboundPolicyItem",
          "sessionStore"
        ]
      },
      "LinkName": {
        "type": "string",
        "description": "link names",
        "enum": [
          "member",
          "memberOf",
          "allowedTo",
          "accessTo",
          "manages",
          "managedBy",
          "one",
          "sub"
        ]
      },
      "LinkType": {
        "$ref": "#/components/schemas/ObjectType",
        "description": "link types"
      },
      "AttributeName": {
        "type": "string",
        "description": "attribute names",
        "enum": [
          "metadata",
          "jwks",
          "registration"
        ]
      }
    },
    "parameters": {
      "objectTypeParam": {
        "in": "path",
        "name": "objectType",
        "required": true,
        "schema": {
          "$ref": "#/components/schemas/ObjectType"
        },
        "description": "object type"
      },
      "objectIdParam": {
        "in": "path",
        "name": "/objectId*",
        "required": true,
        "schema": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "description": "object id"
      },
      "linkNameParam": {
        "in": "path",
        "name": "linkName",
        "required": true,
        "schema": {
          "$ref": "#/components/schemas/LinkName"
        },
        "description": "link name"
      },
      "linkTypeParam": {
        "in": "path",
        "name": "linkType",
        "required": true,
        "schema": {
          "$ref": "#/components/schemas/LinkType"
        },
        "description": "link type"
      },
      "linkIdParam": {
        "in": "path",
        "name": "/linkId*",
        "required": true,
        "schema": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "description": "link id"
      },
      "attributeNameParam": {
        "in": "path",
        "name": "attributeName",
        "required": true,
        "schema": {
          "$ref": "#/components/schemas/AttributeName"
        },
        "description": "attribute name"
      }
    }
  },
  "paths": {
    "/{objectType}": {
      "get": {
        "tags": [
          "objectType"
        ],
        "operationId": "getObjectType",
        "parameters": [
          {
            "$ref": "#/components/parameters/objectTypeParam"
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          }
        }
      }
    },
    "/{objectType}/$link/{linkName}": {
      "get": {
        "tags": [
          "objectType",
          "linkName"
        ],
        "operationId": "getLinkName",
        "parameters": [
          {
            "$ref": "#/components/parameters/objectTypeParam"
          },
          {
            "$ref": "#/components/parameters/linkNameParam"
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          }
        }
      }
    },
    "/{objectType}/$link/{linkName}/{linkType}": {
      "get": {
        "tags": [
          "objectType",
          "linkName",
          "linkType"
        ],
        "operationId": "getLinkNameType",
        "parameters": [
          {
            "$ref": "#/components/parameters/objectTypeParam"
          },
          {
            "$ref": "#/components/parameters/linkNameParam"
          },
          {
            "$ref": "#/components/parameters/linkTypeParam"
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          }
        }
      }
    },
    "/{objectType}/$link/{linkName}/{linkType}/{/linkId*}": {
      "get": {
        "tags": [
          "objectType",
          "linkName",
          "linkType",
          "linkId"
        ],
        "operationId": "getLinkNameTypeId",
        "parameters": [
          {
            "$ref": "#/components/parameters/objectTypeParam"
          },
          {
            "$ref": "#/components/parameters/linkNameParam"
          },
          {
            "$ref": "#/components/parameters/linkTypeParam"
          },
          {
            "$ref": "#/components/parameters/linkIdParam"
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          }
        }
      }
    },
    "/{objectType}/$link/{linkType}": {
      "get": {
        "tags": [
          "objectType",
          "linkType"
        ],
        "operationId": "getLinkType",
        "parameters": [
          {
            "$ref": "#/components/parameters/objectTypeParam"
          },
          {
            "$ref": "#/components/parameters/linkTypeParam"
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          }
        }
      }
    },
    "/{objectType}/$link/{linkType}/{/linkId*}": {
      "get": {
        "tags": [
          "objectType",
          "linkType",
          "linkId"
        ],
        "operationId": "getLinkTypeId",
        "parameters": [
          {
            "$ref": "#/components/parameters/objectTypeParam"
          },
          {
            "$ref": "#/components/parameters/linkTypeParam"
          },
          {
            "$ref": "#/components/parameters/linkIdParam"
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          }
        }
      }
    },
    "/{objectType}/$attribute/{atributeName}": {
      "get": {
        "tags": [
          "objectType",
          "atributeName"
        ],
        "operationId": "getAttributeName",
        "parameters": [
          {
            "$ref": "#/components/parameters/objectTypeParam"
          },
          {
            "$ref": "#/components/parameters/attributeNameParam"
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          }
        }
      }
    },
    "/{objectType}/{/objectId*}": {
      "get": {
        "tags": [
          "objectType",
          "objectId"
        ],
        "operationId": "getObjectId",
        "parameters": [
          {
            "$ref": "#/components/parameters/objectTypeParam"
          },
          {
            "$ref": "#/components/parameters/objectIdParam"
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          }
        }
      }
    },
    "/{objectType}/{/objectId*}/$link/{linkName}": {
      "get": {
        "tags": [
          "objectType",
          "objectId",
          "linkName"
        ],
        "operationId": "getObjectLinkName",
        "parameters": [
          {
            "$ref": "#/components/parameters/objectTypeParam"
          },
          {
            "$ref": "#/components/parameters/objectIdParam"
          },
          {
            "$ref": "#/components/parameters/linkNameParam"
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          }
        }
      }
    },
    "/{objectType}/{/objectId*}/$link/{linkName}/{linkType}": {
      "get": {
        "tags": [
          "objectType",
          "objectId",
          "linkName",
          "linkType"
        ],
        "operationId": "getObjectLinkNameType",
        "parameters": [
          {
            "$ref": "#/components/parameters/objectTypeParam"
          },
          {
            "$ref": "#/components/parameters/objectIdParam"
          },
          {
            "$ref": "#/components/parameters/linkNameParam"
          },
          {
            "$ref": "#/components/parameters/linkTypeParam"
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          }
        }
      }
    },
    "/{objectType}/{/objectId*}/$link/{linkName}/{linkType}/{/linkId*}": {
      "get": {
        "tags": [
          "objectType",
          "objectId",
          "linkName",
          "linkType",
          "linkId"
        ],
        "operationId": "getObjectLinkNameTypeId",
        "parameters": [
          {
            "$ref": "#/components/parameters/objectTypeParam"
          },
          {
            "$ref": "#/components/parameters/objectIdParam"
          },
          {
            "$ref": "#/components/parameters/linkNameParam"
          },
          {
            "$ref": "#/components/parameters/linkTypeParam"
          },
          {
            "$ref": "#/components/parameters/linkIdParam"
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          }
        }
      }
    },
    "/{objectType}/{/objectId*}/$link/{linkType}": {
      "get": {
        "tags": [
          "objectType",
          "objectId",
          "linkType"
        ],
        "operationId": "getObjectLinkType",
        "parameters": [
          {
            "$ref": "#/components/parameters/objectTypeParam"
          },
          {
            "$ref": "#/components/parameters/objectIdParam"
          },
          {
            "$ref": "#/components/parameters/linkTypeParam"
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          }
        }
      }
    },
    "/{objectType}/{/objectId*}/$link/{linkType}/{/linkId*}": {
      "get": {
        "tags": [
          "objectType",
          "objectId",
          "linkType",
          "linkId"
        ],
        "operationId": "getObjectLinkTypeId",
        "parameters": [
          {
            "$ref": "#/components/parameters/objectTypeParam"
          },
          {
            "$ref": "#/components/parameters/objectIdParam"
          },
          {
            "$ref": "#/components/parameters/linkTypeParam"
          },
          {
            "$ref": "#/components/parameters/linkIdParam"
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          }
        }
      }
    },
    "/{objectType}/{/objectId*}/$attribute/{atributeName}": {
      "get": {
        "tags": [
          "objectType",
          "objectId",
          "atributeName"
        ],
        "operationId": "getObjectAttributeName",
        "parameters": [
          {
            "$ref": "#/components/parameters/objectTypeParam"
          },
          {
            "$ref": "#/components/parameters/objectIdParam"
          },
          {
            "$ref": "#/components/parameters/attributeNameParam"
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          }
        }
      }
    }
  }
}